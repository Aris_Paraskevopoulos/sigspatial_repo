package gr.unipi.ds.msc;

import java.io.IOException;
import java.text.ParseException;

import gr.unipi.ds.msc.utils.KeyTypes;
import gr.unipi.ds.msc.utils.Params;

/**
 * The class that contains the entry point of the job
 */
public class App {
	
	/**
	 * Prints the description of the expected command line arguments
	 */
	private static void printUsage() {
		System.out.println("Expecting four command line arguments:");
		System.out.println("Argument 1: Path to input directory");
		System.out.println("Argument 2: Path to output file");
		System.out.println("Argument 3: Cell size in degrees");
		System.out.println("Argument 4: Time step size in days");
	}
	
	/**
	 * The main entry point of the application
	 * @param args The command line arguments used to execute this application
	 * @throws ParseException
	 * @throws IOException
	 */
	public static void main(String[] args) throws ParseException, IOException {
		//Initialize the job params with default values
		String pathToInput = "sigspatial";
		String pathToOutput = "output/sigspatial";
		double cellSizeInDegrees = 0.001d;
		double timeStepSize = 7d;
		
		if (args.length == 4) {
			// parse command line arguments
			pathToInput = args[0];
			pathToOutput = args[1];
			cellSizeInDegrees = Double.parseDouble(args[2]);
			timeStepSize = Double.parseDouble(args[3]);
			
			String dateMin = "2015-01-01 00:00:00";  //minimum date of interest according to the bounding box
			String dateMax = "2016-01-01 00:00:00";  //maximum date of interest according to the bounding box
			//Initialize a Params object using the given values
			Params params = new Params(dateMin, dateMax, cellSizeInDegrees, timeStepSize, KeyTypes.Long);
			//Start job processing
			HotspotAnalysisSpark.analyze(pathToInput, pathToOutput, params);
		}
		else {
			//args.length != 4
			printUsage();
		}
	}
}
