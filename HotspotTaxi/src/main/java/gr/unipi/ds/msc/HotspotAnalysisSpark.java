package gr.unipi.ds.msc;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.spark.Accumulable;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.broadcast.Broadcast;

import scala.Tuple2;
import gr.unipi.ds.msc.utils.Params;
import gr.unipi.ds.msc.utils.LongAccumulator;
import gr.unipi.ds.msc.utils.Statistics;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class HotspotAnalysisSpark {
	
	public static void analyze(String inputPath, String outputPath, Params params) throws ParseException, IOException {
		//Create a new SparkConf object and set AppName to "Hotspot Analysis"
		SparkConf conf = new SparkConf().setAppName("Hotspot Analysis");
		//Get the JavaSparkContext object
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		//Get the distributed file system in order to create the output file
		FileSystem fs = FileSystem.get(sc.hadoopConfiguration());
		//Create the output text file in outputPath using UTF-8 format
		BufferedWriter output = new BufferedWriter(new OutputStreamWriter(fs.create(new Path(outputPath)), "UTF-8"));

		try {
			//If inputPath does not end with a slash char, then append it
			if (inputPath.charAt(inputPath.length() - 1) != '/') {
				inputPath += '/';
			}
			//Append a star char to inputPath in order to read all the input files in the given directory
			inputPath += '*';
			// Broadcast Params object representing the input arguments
			Broadcast<Params> broadcastParams = sc.broadcast(params);
			
			// Load input files as an RDD
			JavaRDD<String> file = sc.textFile(inputPath);
			
			// Initialize accumulators that will help in calculating the mean and standard deviation
			final Accumulable<Long, Long> accum1 = sc.accumulable(0L, new LongAccumulator());
			final Accumulable<Long, Long> accum2 = sc.accumulable(0L, new LongAccumulator());
			
			// Map values to pair RDD, then reduce by key, keep reduced RDD in memory
			// The resulting RDD is the cube described by the cell's id and population (i.e. number of passengers)
			JavaPairRDD<Long, Integer> rawGrid = CubeBuilder.readFileLongKey(file, broadcastParams);
			JavaPairRDD<Long, Integer> firstGrid = CubeBuilder.reduceLong(rawGrid);
			
			//Calculate the values of accumulators
			//accum1 holds for the sum of passengers
			//accum2 holds for the squared sum of passengers
			firstGrid.foreach(new VoidFunction<Tuple2<Long, Integer>>() {
				private static final long serialVersionUID = 7187811695996133115L;

				public void call(Tuple2<Long, Integer> t) throws Exception {
					accum1.add((long) t._2);
					accum2.add((long) t._2 * t._2);
				}

			});

			// calculate statistics and broadcast them (i.e. mean and standard deviation)
			Statistics statistics = new Statistics(accum1.value(), accum2.value(), params);
			Broadcast<Statistics> broadcastStatistics = sc.broadcast(statistics);

			// Convert the cube RDD to neighbor cube RDD, by each cell's population with its neighbors' population
			JavaPairRDD<Long, Integer> neighborRawGrid = NeighborCubeBuilder.generateNeighborLongKey(firstGrid, broadcastParams);
			JavaPairRDD<Long, Integer> neighborGrid = NeighborCubeBuilder.reduceLong(neighborRawGrid);

			// Convert the neighbor cube RDD to zScore cube RDD, by calculating the zScore of each Cell
			JavaPairRDD<Double, String> scoreGrid = ScoreCubeBuilder.generateScoreCubeLongKey(neighborGrid, broadcastParams, broadcastStatistics);
			
			// sort by key (zScore) descending
			JavaPairRDD<Double, String> sortedGrid = scoreGrid.sortByKey(false).cache();

			// output top50 Cells in csv format to output file
			List<Tuple2<Double, String>> top50 = new ArrayList<Tuple2<Double, String>>();
			top50 = sortedGrid.take(50);
			for (int i = 0; i < 50; i++) {
				output.write(top50.get(i)._2 + "," + top50.get(i)._1 + ","
						+ getPValue(top50.get(i)._1, statistics.passengerMean, statistics.standardDeviation)
						+ "\n");
			}
		} finally {
			output.close();
			fs.close();
		}
		sc.close();
		sc.stop();

	}

	/**
	 * Calculate the pValue by a zScore
	 * @param zScore the zScore used to calculate the pValue
	 * @return the pValue of the given zScore
	 */
	public static double getPValue(double zScore, double mean, double sd) {
		NormalDistribution d = new NormalDistribution(mean, sd);
		return (1 - d.cumulativeProbability(Math.abs(zScore))) * 2;
	}
}