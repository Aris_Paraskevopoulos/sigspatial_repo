package gr.unipi.ds.msc.utils;

import java.util.Comparator;
import scala.Tuple2;

/**
 * Comparator class for the PriorityQueue sorting
 */
public class TupleComparator implements Comparator<Tuple2<Double, String>> {

	/**
	 * Overridden method compare
	 * @param a A tuple2 already in a PriorityQueue
	 * @param b A tuple2 to be added in a PriorityQueue
	 * @return A positive, zero or negative integer to sort PriorityQueue ascending
	 */
	@Override
	public int compare(Tuple2<Double, String> a, Tuple2<Double, String> b) {
		if (a._1 < b._1) {
			return 1;
		}
		if (a._1 > b._1) {
			return -1;
		}
		return 0;
	}

}
