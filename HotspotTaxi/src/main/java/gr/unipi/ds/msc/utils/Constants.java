package gr.unipi.ds.msc.utils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Constants implements Serializable {
	private static final long serialVersionUID = 203031547741399818L;
	public  long dateMin;
	public  long dateMax;
	public  int dateSegments;  //total number of date grid segments
	public  double dateStep; //date step
	public  final double dayInMillis = 86400000d; // a day in millisecond
	public 	final double longitudeMin = -74.25d;
	public	final double longitudeMax = -73.7d;
	public  int longitudeSegments; //total number of longitude and latitude grid segments
	public  int latitudeSegments;
	public	final double latitudeMin = 40.5; 
	public	final double latitudeMax = 40.9;
	public  double cellSize;
	public 	long maxX;
	public 	long minX;
	public 	long maxY;
	public 	long minY;
	
	public Constants(double cellSize, double timeStepSize) throws ParseException {
		SimpleDateFormat parserSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateMin = parserSDF.parse("2015-12-01 00:00:00").getTime();
		dateMax = parserSDF.parse("2016-01-01 00:00:00").getTime();
		
		//calculate date steps and total date segments
		dateStep = dayInMillis*timeStepSize;
		dateSegments = (int) Math.ceil((dateMax - dateMin)/dateStep);
		
		//calculate new long and lat bounds (calculating from point with long = 0 and lat = 0)
		
		//calculate min and max point(x,y) values and total number of segments 
		longitudeSegments = (int) Math.ceil((longitudeMax-longitudeMin)/cellSize);
		latitudeSegments = (int) Math.ceil((latitudeMax-latitudeMin)/cellSize);
		this.cellSize=cellSize;
		maxX=(long)Math.ceil(longitudeMax/cellSize);
		minX=(long)Math.floor(longitudeMin/cellSize);
		maxY=(long)Math.ceil(latitudeMax/cellSize);
		minY=(long)Math.ceil(latitudeMin/cellSize);
		
	}
}
